package com.example.service;

import java.util.List;

import com.example.model.CourseModel;

public interface CourseService
{
    CourseModel selectCourse (String id_course);


    List<CourseModel> selectAllCourse();


    void addCourse (CourseModel course);


    void deleteCourse (String id_course);
    
    void updateCourse (CourseModel course);
}