package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Many;

import com.example.model.CourseModel;

@Mapper
public interface CourseMapper {

    @Select("select id_course, nama, sks from course where id_course = #{id_course}")
    @Results(value = {
        @Result(property = "id_course", column = "id_course"),
        @Result(property = "nama", column = "nama"),
        @Result(property = "sks", column = "sks"),
        @Result(property = "students", column = "id_course", javaType = List.class, many = @Many(select = "selectStudents"))})
    CourseModel selectCourse(@Param("id_course") String id_course);

    @Select("select id_course, nama, sks from course")
    @Results(value = {
        @Result(property = "id_course", column = "id_course"),
        @Result(property = "nama", column = "nama"),
        @Result(property = "sks", column = "sks"),
        @Result(property = "students", column = "id_course", javaType = List.class, many = @Many(select = "selectStudents"))})
    List<CourseModel> selectAllCourse();
    
}
