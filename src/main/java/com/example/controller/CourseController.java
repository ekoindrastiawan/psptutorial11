package com.example.controller;

import com.example.model.CourseModel;
import com.example.service.CourseService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CourseController {

    @Autowired
    CourseService courseDAO;

    @RequestMapping("/course/viewall")
    public String view(Model model) {
        List<CourseModel> courses = courseDAO.selectAllCourse();
        model.addAttribute("courses", courses);

        return "viewall-courses";
    }

}
